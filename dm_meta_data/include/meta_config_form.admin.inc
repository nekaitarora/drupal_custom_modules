<?php

/**
 * @file
 * DM meta data config form.
 */

function dm_meta_data_config_form($form, &$form_state) {
    $form = array();
    $form['api_key'] = array(
        '#title' => t('API KEY'),
        '#type'  => 'textfield',
        '#default_value' => variable_get('api_key', '')
    );

    $form['api_secret'] = array(
        '#title' => t('API SECRET'),
        '#type'  => 'textfield',
        '#default_value' => variable_get('api_secret', '')
    );

    $form['api_username'] = array(
        '#title' => t('USERNAME'),
        '#type'  => 'textfield',
        '#default_value' => variable_get('api_username', '')
    );

    $form['api_password'] = array(
        '#title' => t('PASSWORD'),
        '#type'  => 'textfield',
        '#default_value' => variable_get('api_password', '')
    );

    $form['user_no_dropdoen'] = array(
        '#title' => t('Listing No.'),
        '#type'  => 'textfield',
        '#default_value' => variable_get('user_no_dropdoen', '')
    );

    return system_settings_form($form);
}