<?php
/**
 * @file
 * Dashboard setting file.
 */

/**
 * Implementations of set_dashboard_form().
 */
function set_dashboard_form() {
  $form['admin_dashboard'] = array(
    '#type' => 'textfield',
    '#title' => t('Set DM-admin Dashboard :'),
    '#default_value' => variable_get('admin_dashboard', 'user'),
  );
  $form['company_dashboard'] = array(
    '#type' => 'textfield',
    '#title' => t('Set Company Dashboard :'),
    '#default_value' => variable_get('company_dashboard', 'user'),
  );
  $form['company_user_dashboard'] = array(
    '#type' => 'textfield',
    '#title' => t('Set Company-User Dashboard :'),
    '#default_value' => variable_get('company_user_dashboard', 'user'),
  );

  return system_settings_form($form);
}
